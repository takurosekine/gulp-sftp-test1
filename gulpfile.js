var gulp = require('gulp');
var fs   = require('fs');
var sftp = require('gulp-sftp');


// FTP info
// @see http://mpon.hatenablog.com/entry/2014/03/10/001214
var server_info = JSON.parse(fs.readFileSync('ftp.json', 'utf8'));


// ----------------------------------------
// upload only
// ----------------------------------------
gulp.task('deploy', function () {
	return gulp.src( 'html/**/*' )
		.pipe(sftp( server_info )
	);
});


gulp.task('default', ['deploy']);