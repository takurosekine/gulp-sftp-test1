# gulp-sftp Test1

## ブランチについて

### master

ここ。
ファイルをアップロードするだけのタスク。

### upload-and-reload

ファイルを監視して、変更されたら自動でアップロード＆リロードされるタスク。
現状、変更されたファイル以外も全てアップロードしてしまう。



## Install

```
npm install
```

## Input server info

edit `ftp.json`

```
{
	"host": "example.com",
	"user": "yamada",
	"pass": "********",
	"remotePath": "/www/***/"
}
```

## Deploy

```
gulp
```

## My Version

* `node -v` : v5.2.0
* `npm -v` : 3.3.12